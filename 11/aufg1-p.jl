# addprocs(3)

@everywhere using Distributions
@everywhere using StatsBase



@everywhere function KS_Alpha(alpha::Float64)
    K_alpha = sqrt(log(2/alpha) / 2)
    return K_alpha
end


@everywhere function KS_Wert(lam::Float64, bins::Int64, N::Int64)
    mu = lam
    sigma = sqrt(lam)
    untergrenze = mu - 5*sigma
    obergrenze = mu + 5*sigma

    f_x = rand(Poisson(lam), N)
    hist_x = fit(Histogram, f_x, linspace(untergrenze, obergrenze, bins+1))
    cum_x = cumsum(hist_x.weights)
    cum_x /= cum_x[end]  # normieren

    f_0 = round(rand(Normal(mu, sigma), N))
    hist_0 = fit(Histogram, f_0, linspace(untergrenze, obergrenze, bins+1))
    cum_0 = cumsum(hist_0.weights)
    cum_0 /= cum_0[end]

    # KS Wert berechnen
    diff = abs(f_x - f_0)
    d_max = maximum(diff)
    n = N
    m = N
    kswert = sqrt((n*m)/(n+m)) * d_max
    return kswert
end


@everywhere function calcy(lam::Float64)
    ks = 0
    N = 10000
    bins = 100
    for j=1:200
        ks += test(lam, bins, N)
    end
    return ks
end


@everywhere function calc()
    size = 100
    lambdas = linspace(0.1, 20, size)
    
    # for (i, lam) in enumerate(lambdas)
    #     ks[i] = @parallel (+) for j=1:200
    #         test(lam, bins, N)
    #     end
    # end
    # ks = SharedArray(Float64, size)
    # @parallel for (i, lam) in enumerate(lambdas)
    #     ks[i] = 0
    #     for j=1:200
    #         ks[i] += test(lam, bins, N)
    #     end
    # end
    ks = pmap(calcy, lambdas)

    # ks /= 5
    return ks
end


print(calc())
