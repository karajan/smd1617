import numpy as np
import matplotlib.pyplot as plt

plt.style.use("ggplot")



def KS_Wert(f_x, f_0):
    diff = np.abs(f_x - f_0)
    d_max = np.max(diff)  # Maximum der Differenz
    n = N
    m = N
    testwert = np.sqrt((n*m)/(n+m)) * d_max  # berechnet nach Vorlesung
    return testwert


def KS_Alpha(alpha):
    K_alpha = np.sqrt(np.log(2/alpha) / 2)  # berechnet nach Vorlesung
    return K_alpha


def test(lam):
    mu = lam
    sigma = np.sqrt(lam)
    untergrenze = mu - 5*sigma
    obergrenze = mu + 5*sigma

    f_x = np.random.poisson(lam, N)
    # Histogram mit 100 Bins und Unter- und Obergrenze
    hist_x = np.histogram(f_x, bins, (untergrenze, obergrenze))
    cum_x = np.cumsum(hist_x[0])  # kumuliert
    cum_x = cum_x / cum_x[-1]  # normieren

    f_0 = np.floor(np.random.normal(mu, sigma, N) + 0.5)
    hist_0 = np.histogram(f_0, bins, (untergrenze, obergrenze))
    cum_0 = np.cumsum(hist_0[0])
    cum_0 = cum_0 / cum_0[-1]
    
    return KS_Wert(cum_x, cum_0)



N = 10000
bins = 100
size = 100
lambdas = np.linspace(0.1, 20, size)

ks = np.zeros(size)
# Ohne mehr Iterationen lässt sich quasi unmöglich ein Wert ablesen, da die
# Kurve durch die Zufallszahlen zu sehr schwankt
iterations = 500
for i, lam in enumerate(lambdas):
    for j in range(iterations):
        ks[i] += test(lam)
ks /= iterations

plt.plot(lambdas, ks)
plt.plot(lambdas, np.full_like(lambdas, KS_Alpha(0.05)), label="0.05")
plt.plot(lambdas, np.full_like(lambdas, KS_Alpha(0.025)), label="0.025")
plt.plot(lambdas, np.full_like(lambdas, KS_Alpha(0.001)), label="0.001")

plt.legend()
plt.savefig("plots/01.pdf", bbox_inches="tight")
plt.show()
plt.clf()

