#! /usr/bin/env python
# -*- coding: UTF-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from sympy.solvers import solve
from sympy import Symbol
from mpmath import *

plt.style.use("ggplot")



''' a) '''

def log_fact(x):
    return np.log(np.math.factorial(x))


def neg_log_llh(l):
    return 3 * l - 30 * np.log(l) + log_fact(8) + log_fact(9) + log_fact(13)


l = np.linspace(0.1, 30)
plt.plot(l, neg_log_llh(l))
plt.xlabel('$\lambda$')
plt.ylabel('-log(L)')
# plt.savefig("plots/03a.pdf", bbox_inches="tight")
# plt.show()
plt.clf()

''' b) '''
# Minimum liegt bei l=10


# (neg_log_llh(10) = 6.88104144613)



''' c) '''

# Gefunden mit Wolfi

# l_(1/2)_1 ≈ 8.28363715275211
# l_(1/2)_2 ≈ 11.9385028086121

# l_(4/2)_1 ≈ 6.77876488129732
# l_(4/2)_2 ≈ 14.1088099530048

# l_(9/2)_1 ≈ 5.47370348774750
# l_(9/2)_2 ≈ 16.5196623760468

# Aussage:
# Beschreiben Konfidenzintervalle
# Ersten beiden Werte beschreiben 1 sigma-Umgebung



''' d) '''

def nlllh_taylor(l):
    return 3 * l - 30 * ((l-10) - (l-10)**2/2) + log_fact(8) + log_fact(9) + log_fact(13)


l = np.linspace(0.1, 15)
plt.plot(l, neg_log_llh(l),label='negative Log-Likelihood')
plt.plot(l, nlllh_taylor(l),label='Taylor-Entwicklung')
plt.xlabel('$\lambda$')
plt.ylabel('-log(L)')
plt.legend(loc='best')

# plt.savefig("plots/03d.pdf", bbox_inches="tight")
plt.show()
plt.clf()


