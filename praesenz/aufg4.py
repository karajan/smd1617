#! /usr/bin/env python3
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import ROOT
import root_numpy


# root_file = ROOT.TFile("praesenz.root", "RECREATE")
# tree = ROOT.TTree("random-numbers", "random-numbers")

a = np.empty((1000, ), dtype=[("x", float), ("y", float), ("z", int)])
x = np.random.rand(1000) * 1000
y = np.random.normal(x, np.sqrt(x), 1000)
z = np.random.poisson(x, 1000)
a["x"] = x 
a["y"] = y 
a["z"] = z 

root_numpy.array2root(a, "praesenz.root", treename="tree", mode="RECREATE")
