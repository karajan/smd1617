#! /usr/bin/env python
# -*- coding: UTF-8 -*-

import numpy as np
import matplotlib.pyplot as plt

plt.style.use("ggplot")


''' a) '''

x_min = 3
x_max = 8

def a(x):
    return 1 / (x_max - x_min) * x / x


def A_inv(z):
    return z * (x_max - x_min) + x_min



z = np.random.rand(1000000)
x = np.linspace(x_min, x_max, 50)

plt.hist(A_inv(z), bins=50, normed=1)
plt.plot(x, a(x))

plt.savefig("plots/03a.pdf", bbox_inches="tight")
# plt.show()


''' b) '''

def b(t, tau):
    return 1/tau * np.exp(-t/tau)


def B_inv(z, tau):
    return -tau * np.log(z)


z = np.random.rand(1000000)
t = np.linspace(0, 10)

plt.hist(B_inv(z, 1), bins=50, normed=1)
plt.plot(t, b(t, 1))

plt.savefig("plots/03b.pdf", bbox_inches="tight")
# plt.show()


''' c) '''

n = 3
x_min = 3
x_max = 8

def N():
    return (1 - n) / (x_max**(1-n) - x_min**(1-n))


def c(x):
    return N() * x**(-n)


def C(z):
    return N() / (1-n) * -z**(1-n)


def C_inv(z):
    return np.power((1-n) / N() * -z, 1/(1-n))


z = np.random.rand(1000000) * (C(x_max) - C(x_min)) + C(x_min)
t = np.linspace(x_min, x_max)

plt.plot(t, c(t))
plt.hist(C_inv(z), bins=50, normed=1)

plt.savefig("plots/03c.pdf", bbox_inches="tight")
# plt.show()


''' d) '''

def d(x):
    return 1/np.pi * 1/(1 + x**2)


def D_inv(z):
    return np.tan(np.pi * -z)


x = np.linspace(-20, 20)
z = np.random.rand(1000000)

D = D_inv(z)
D = D[D > -20]
D = D[D < 20]

plt.hist(D, bins=50, normed=1)
plt.plot(x, d(x))
plt.xlim(-20, 20)

plt.savefig("plots/03d.pdf", bbox_inches="tight")
# plt.show()


''' e) '''

size = 1000000
values = np.random.rand(size)

data = np.load("empirisches_histogramm.npy")
hist = data["hist"]
bins = data["bin_mid"]

weights = hist / np.sum(hist)  # Gewichte normieren
# die Bins verteilt nach Häufigkeit ziehen
rands = np.random.choice(bins, size, p=weights)
# wir haben jetzt nur die Bins gezogen. Um die Zahlen zwischen den einzelnen
# Bins gleichzuverteilen, diese Verschiebung
verschiebungen = np.random.rand(size) * 0.02 - 0.01
rands += verschiebungen

# Der originale Plot für das Histogramm erzeugt einen hohen Peak, der so nicht
# vorhanden ist, daher die Änderung
plt.hist(bins, bins=np.linspace (0., 1., 51), weights=hist,
                                    normed=1.0, alpha=0.5, color="b")
plt.hist(rands, bins=200, normed=1.5, alpha=0.5, color="r")

plt.savefig("plots/03e.pdf", bbox_inches="tight")
# plt.show()
