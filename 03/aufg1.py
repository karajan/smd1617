#! /usr/bin/env python
# -*- coding: UTF-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import root_numpy

plt.style.use("ggplot")



''' a) '''

gamma = 2.7

def phi(E):
    return E**(-gamma) * (gamma - 1)


# Wie üblich berechnet, siehe b02a02.
def Phi_inv(z):
    return (1 - z)**(1/(1-gamma))


z = np.random.rand(100000)
energien = Phi_inv(z)

data = np.empty((100000, ), dtype=[("Energie", float)])
data["Energie"] = energien
root_numpy.array2root(data, "NeutrinoMC.root", treename="Signal_MC",
                                        mode="recreate")



''' b) '''

def P(E):
    return (1 - np.exp(-E/2))**3


# Es werden alle Energien akzeptiert, bei der der Zufallswert kleiner als
# die berechnete Akzeptanz ist.
akzeptanz = P(energien)
energien_a = energien[akzeptanz > np.random.rand(100000)]


data = np.empty((len(energien_a), ), dtype=[("Energie", float)])
data["Energie"] = energien_a
root_numpy.array2root(data, "NeutrinoMC.root", treename="Signal_MC_Akzeptanz",
                                        mode="update")


def plot_b():
    x = np.logspace(0.1, 4, 50)
    plt.hist(energien, bins=x)
    plt.hist(energien_a, bins=x)
    plt.xscale("log")
    plt.yscale("log")

    plt.savefig("plots/01ab.pdf", bbox_inches="tight")
    # plt.show()
    plt.clf()


plot_b()



''' c) '''

# Ich habe das schon mal erwähnt, aber in der Vorlesung wird nicht die
# Polarmethode sondern die Box Muller Methode gemacht. Hier die Polarmethode
# (von Wikipedia).
def polar(n):
    # Was genau ich hier treibe erkläre ich unten.
    size = np.rint(n/2 + 0.5).astype(int)
    u = np.random.rand(size) * 2 - 1
    v = np.random.rand(size) * 2 - 1
    q = u**2 + v**2
    indices = np.where((q == 0) | (q >= 1))
    i_size = len(indices[0])
    while i_size > 0:
        u[indices] = np.random.rand(i_size)
        v[indices] = np.random.rand(i_size)
        q = u**2 + v**2
        indices = np.where((q == 0) | (q >= 1))
        i_size = len(indices[0])
    p = np.sqrt((-2 * np.log(q)) / q)
    # Ich bekomme für zwei gleichverteilte Zufallszahlen immer zwei normal-
    # verteilte Zufallszahlen. Hier schaue ich, dass ich die richtige
    # Anzahl nehme.
    return np.concatenate((p * u, p[0:n-size] * v[0:n-size]))


size = len(energien_a)
# Hits nach Formel berechnen.
hits = polar(size) * (2 * energien_a) + (1 * energien_a)
# Finde Indices, wo die Anzahl der Hits kleiner 0.
indices = np.where(hits < 0)
i_size = len(indices[0])
# Alle Hits müssen größer 0, also die Anzahl der Indices = 0.
while i_size > 0:
    # An den Stellen, wo die Hits < 0 berechne ich die Hits neu ...
    hits[indices] = polar(i_size) * (2 * energien_a[indices]) + (1 * energien_a[indices])
    # ... und berechne danach die Indices, wo Hits < 0, neu.
    indices = np.where(hits < 0)
    i_size = len(indices[0])
# This is some thight numpy-shit right here, okay. Ja, mit einer Schleife
# wäre es einfacher, aber ich kann's halt :-D


data = np.empty((len(hits), ), dtype=[("AnzahlHits", int)])
data["AnzahlHits"] = hits
root_numpy.array2root(data, "NeutrinoMC.root", treename="Signal_MC_Akzeptanz",
                                        mode="update")



''' d) '''

# Berechnung der Orte für verschiedene Sigma-Werte. Liste der Orte hat die
# gleiche Länge wie die der Sigmas.
# Algorithmus siehe oben, nur mit anderer Bedingung für die Indices.
def ort(mean=0, sigma=1, min_wert=-np.inf, max_wert=np.inf):
    ort = np.random.normal(mean, sigma)
    indices = np.where((ort < min_wert) | (ort > max_wert))
    i_size = len(indices[0])
    while i_size > 0:
        ort[indices] = np.random.normal(mean, sigma[indices], i_size)
        indices = np.where((ort < min_wert) | (ort > max_wert))
        i_size = len(indices[0])
    return ort


sigma = 1 / np.log10(hits + 1)
x = ort(7, sigma, 0, 10)
y = ort(3, sigma, 0, 10)


data = np.empty(len(hits), dtype=[("x", float), ("y", float)])
data["x"] = x
data["y"] = y
root_numpy.array2root(data, "NeutrinoMC.root", treename="Signal_MC_Akzeptanz",
                                        mode="update")


def plot_d():
    plt.hist2d(x, y, bins=50, norm=LogNorm())
    plt.colorbar()

    plt.savefig("plots/01d.pdf", bbox_inches="tight")
    # plt.show()
    plt.clf()


plot_d()



''' e) '''

# Anstatt die Anzahl der Orte über die Sigmas festzulegen, stattdessen mit
# Parameter size.
def ort(mean=0, sigma=1, min_wert=-np.inf, max_wert=np.inf, size=1):
    ort = np.random.normal(mean, sigma, size)
    indices = np.where((ort < min_wert) | (ort > max_wert))
    i_size = len(indices[0])
    while i_size > 0:
        ort[indices] = np.random.normal(mean, sigma, i_size)
        indices = np.where((ort < min_wert) | (ort > max_wert))
        i_size = len(indices[0])
    return ort


# Berechnung der Orte unter Berücksichtigung der Korrelation. Ich berechne erst
# das schon verteilte y. Deshalb muss ich "+ mean" durch "+ (1-korr) * mean"
# ersetzen.
def ort_k(korrwith, korr=0, mean=0, sigma=1, min_wert=-np.inf, max_wert=np.inf):
    def calc_korr(x, y, korr, mean, sigma):
        return np.sqrt(1 - korr**2) * sigma * x + korr * y + (1 - korr) * mean

    size = len(korrwith)
    ort = np.random.randn(size)
    ort = calc_korr(ort, korrwith, korr, mean, sigma)
    indices = np.where((ort < min_wert) | (ort > max_wert))
    i_size = len(indices[0])
    while i_size > 0:
        ort_n = np.random.randn(i_size)
        ort[indices] = calc_korr(ort_n, korrwith[indices], korr, mean, sigma)
        indices = np.where((ort < min_wert) | (ort > max_wert))
        i_size = len(indices[0])
    return ort


N_ugr = 10000000
hits_ugr = (np.power(10, np.random.normal(2, 1, N_ugr)) + 0.5).astype(int)

y_u = ort(5, 5, 0, 10, N_ugr)
x_u = ort_k(y_u, 0.5, 5, 5, 0, 10)


data = np.empty(N_ugr, dtype=[("AnzahlHits", int), ("x", float), ("y", float)])
data["AnzahlHits"] = hits_ugr
data["x"] = x_u
data["y"] = y_u
root_numpy.array2root(data, "NeutrinoMC.root", treename="Untergrund_MC",
                                        mode="update")


def plot_e():
    plt.hist2d(x_u, y_u, bins=50)
    plt.colorbar()

    plt.savefig("plots/01e_xy.pdf", bbox_inches="tight")
    # plt.show()
    plt.clf()


    x = np.logspace(0.1, 6, 50)
    plt.hist(hits_ugr, bins=x)
    plt.xscale("log")

    plt.savefig("plots/01e_hits.pdf", bbox_inches="tight")
    # plt.show()
    plt.clf()


plot_e()
