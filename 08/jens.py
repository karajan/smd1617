#! /usr/bin/env python
# -*- coding: UTF-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn.preprocessing import Imputer
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.wrappers.scikit_learn import KerasRegressor


test = pd.read_csv("test.csv")
train = pd.read_csv("train.csv")
saleprice = train["SalePrice"]
saleprice = np.log(saleprice).astype(np.float32)
train.pop("SalePrice");

all_data = pd.concat([train, test])
all_data = pd.get_dummies(all_data, drop_first=True)
imp = Imputer(missing_values="NaN", strategy="mean", axis=0)
imp.fit(all_data)
all_data_clean = imp.transform(all_data).astype(np.float32)

train_cl = all_data_clean[:train.shape[0]]
test_cl = all_data_clean[train.shape[0]:]


# corr = train_cl.corr()


def baseline_model():
  model = Sequential()
  model.add(Dense(500, input_dim=246, init="normal", activation="relu"))
  model.add(Dense(32, init='normal', activation='relu'))
  model.add(Dense(1, init="normal"))
  # Compile model
  model.compile(loss="mean_squared_error", optimizer="adam")
  return model


# estimator = KerasRegressor(build_fn=baseline_model, nb_epoch=100, batch_size=5, verbose=0)


seed = 7
np.random.seed(seed)

estimators = []
estimators.append(('standardize', StandardScaler()))
estimators.append(('mlp', KerasRegressor(build_fn=baseline_model, nb_epoch=50, batch_size=1, verbose=0)))
pipeline = Pipeline(estimators)

# model = baseline_model()
# model.fit(train_cl, saleprice, nb_epoch=200, batch_size=4, verbose=0)

kfold = KFold(n_splits=3, random_state=seed)
score_dl = cross_val_score(pipeline, train_cl, saleprice, cv=kfold, scoring="neg_mean_squared_error")
score_dl = np.sqrt(-score_dl.mean())
print(score_dl)

# predict = model.predict(test_cl)
# final_data = pd.DataFrame([test["Id"], np.exp(predict)[:,0]], index=["Id", "SalePrice"]).T
# final_data.to_csv("submission3.csv", index=False)
