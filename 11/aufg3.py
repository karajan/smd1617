import numpy as np


def create_matrix(N, e):
    A = np.zeros((N, N))
    
    np.fill_diagonal(A, 1-2*e)  # Diagonale
    A[0,0] = 1-e  # links oben
    A[-1,-1] = 1-e  # rechts unten
    diag = np.diagonal(A, 1)  # Nebendiagonalen
    diag.setflags(write=True)  # schmutziger, schutziger Hack. stackoverflow ist Schuld.
    diag.fill(e)
    diag = np.diagonal(A, -1)
    diag.setflags(write=True)
    diag.fill(e)
    
    return A
