import os
import string
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit 
from scipy import constants as c
import csv

######
# b) #
######

#read train data of 3 attributes with best correlation
OverallQual, GarageCars, GrLivArea, SalePrice = np.genfromtxt('a.txt',delimiter=',',unpack=True)
#0.791		,0.709		,0.640 //correlations

#do scatter plots
plt.scatter(OverallQual,SalePrice)
plt.ylabel('SalePrice')
plt.xlabel('OverallQual')
plt.xlim(0,11)
plt.savefig('OverallQual.png',dpi=200)
plt.show()
plt.clf()

plt.scatter(GarageCars,SalePrice)
plt.ylabel('SalePrice')
plt.xlabel('GarageCars')	
plt.xlim(-1,5)
plt.savefig('GarageCars.png',dpi=200)
plt.show()
plt.clf()

plt.scatter(GrLivArea,SalePrice)
plt.ylabel('SalePrice')
plt.xlabel('GrLivArea')
plt.savefig('GrLivArea.png',dpi=200)
plt.show()
plt.clf()

def regression(x, m, b):
	return m*x+b

#do fit
para, cov = curve_fit(regression, OverallQual, SalePrice)
err = np.sqrt(np.diag(cov))
#print(para)
#print(err)

#pllot fit
plt.plot(OverallQual, SalePrice, 'x', label='Messwerte')  
x=np.arange(0,11000)
x=x/1000
plt.plot(x, regression(x, *para), label='Regression')
plt.legend(loc='best')
plt.ylabel('SalePrice')
plt.xlabel('OverallQual')
plt.xlim(0,11)
plt.savefig('regression.png',dpi=200)
plt.show()
plt.clf()

######
# c) #
######
#calculate relative price difference

predprices=[]
relprices=[]

for i in range(0,11):
	predprices=np.append(predprices,regression(i,*para))

for i in range(len(SalePrice)):
	relprices=np.append(relprices,(100*(SalePrice[i]-predprices[int(OverallQual[i])])/(SalePrice[i])))
	
#plot relative price difference
plt.hist(relprices,bins=100)
plt.ylabel('Anzahl')
plt.xlabel('Relativer Preisabstand [%]')
plt.savefig('relativedistances.png',dpi=200)
plt.show()
plt.clf()

######
# d) #
######
#fit regression to train data (use GrLivArea insted of OverallQual)

para, cov = curve_fit(regression, GrLivArea, SalePrice)
err = np.sqrt(np.diag(cov))
#print(para)
#print(err)

#plot regression and train data
plt.plot(GrLivArea, SalePrice, 'x', label='Messwerte')  
x=np.arange(0,6000)
plt.plot(x, regression(x, *para), label='Regression')
plt.legend(loc='best')
plt.ylabel('SalePrice')
plt.xlabel('GrLivArea')
plt.savefig('regression2.png',dpi=200)
plt.show()
plt.clf()

#calculate relative price difference

predprices=[]
relprices=[]

for i in range(0,11):
	predprices=np.append(predprices,regression(i,*para))

for i in range(len(SalePrice)):
	relprices=np.append(relprices,(100*(SalePrice[i]-predprices[int(OverallQual[i])])/(SalePrice[i])))
	
#plot relative price difference
plt.hist(relprices,bins=100)
plt.ylabel('Anzahl')
plt.xlabel('Relativer Preisabstand [%]')
plt.savefig('relativedistances2.png',dpi=200)
plt.show()
plt.clf()

#calculate prices for test data
GrLivArea_test = np.genfromtxt('b.txt',delimiter=',',unpack=True)

SalePrice_test=[]
index=[]

for i in range(len(GrLivArea_test)):
	SalePrice_test=np.append(SalePrice_test,regression(GrLivArea[i], *para))

for i in range(1461,2920):
	index = np.append(index,int(i))

#np.savetxt('sp.txt',SalePrice_test, fmt='%i')
#np.savetxt('id.txt',index, fmt='%i')
