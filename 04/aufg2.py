#! /usr/bin/env python
# -*- coding: UTF-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import root_numpy

plt.style.use("ggplot")



''' a) '''

P_0 = root_numpy.root2array("zwei_populationen.root", "P_0_10000")
P_1 = root_numpy.root2array("zwei_populationen.root", "P_1")


def g1(x):
    return np.zeros_like(x)


def g2(x):
    return -3/4 * x


def g3(x):
    return -5/4 * x


# projezieren auf Gerade a + bx
def proj(b, x, y):
    return (x + b*y) / (1 + b**2)


proj_p0_1x = proj(0, P_0["x"], P_0["y"])
proj_p0_1y = g1(proj_p0_1x)
proj_p0_2x = proj(-0.75, P_0["x"], P_0["y"])
proj_p0_2y = g2(proj_p0_2x)
proj_p0_3x = proj(-5/4, P_0["x"], P_0["y"])
proj_p0_3y = g3(proj_p0_3x)
proj_p1_1x = proj(0, P_1["x"], P_1["y"])
proj_p1_1y = g1(proj_p1_1x)
proj_p1_2x = proj(-0.75, P_1["x"], P_1["y"])
proj_p1_2y = g2(proj_p1_2x)
proj_p1_3x = proj(-5/4, P_1["x"], P_1["y"])
proj_p1_3y = g3(proj_p1_3x)


def plot_a():
    plt.scatter(P_0["x"], P_0["y"], label="P_0", color="r")
    plt.scatter(P_1["x"], P_1["y"], label="P_1", color="b")

    x = np.linspace(-15, 20, 100)
    # plt.xlim(-15, 20)

    # plt.plot(x, g1(x), label="g1(x)")
    # plt.plot(x, g2(x), label="g2(x)")
    plt.plot(x, g3(x), label="g3(x)")

    # plt.scatter(proj_p0_1x, proj_p0_1y, label="P_0_g1", color="y")
    # plt.scatter(proj_p0_2x, proj_p0_2y, label="P_0_g2", color="y")
    plt.scatter(proj_p0_3x, proj_p0_3y, label="P_0_g3", color="y")
    # plt.scatter(proj_p1_1x, proj_p1_1y, label="P_0_g1", color="k")
    # plt.scatter(proj_p1_2x, proj_p1_2y, label="P_0_g2", color="k")
    plt.scatter(proj_p1_3x, proj_p1_3y, label="P_0_g3", color="k")


    plt.axes().set_aspect("equal", "datalim")
    plt.legend()
    plt.savefig("plots/02g3.pdf", bbox_inches="tight")
    plt.show()
    plt.clf()


# plot_a()


def plot_hist():
    # plt.hist(proj_p0_1x, bins=50, alpha=0.7)
    # plt.hist(proj_p1_1x, bins=50, alpha=0.7)
    plt.hist(proj_p0_2x, bins=50, alpha=0.7)
    plt.hist(proj_p1_2x, bins=50, alpha=0.7)
    # plt.hist(proj_p0_3x, bins=50, alpha=0.7)
    # plt.hist(proj_p1_3x, bins=50, alpha=0.7)

    plt.savefig("plots/02h2.pdf", bbox_inches="tight")
    plt.show()
    plt.clf()


plot_hist()
