#! /usr/bin/env python
# -*- coding: UTF-8 -*-

import numpy as np
import matplotlib.pyplot as plt


plt.style.use("ggplot")



''' b) '''

phi = np.arange(0, 360, 30)
phi = phi * 2*np.pi / 360
asymmetrie = np.array([-0.032, 0.010, 0.057, 0.068, 0.076, 0.080, 0.031,
                        0.005, -0.041, -0.090, -0.088, -0.074])

A = np.array([np.cos(phi), np.sin(phi)]).T

a = np.dot(np.dot(np.linalg.inv(np.dot(A.T, A)), A.T), asymmetrie)

