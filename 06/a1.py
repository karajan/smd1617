#Blatt 6 Aufgabe 1

import numpy as np
import matplotlib.pyplot as plt
import root_numpy

# Warning: Program may take a long time. ~1 hour :/
# c)
# Determines euclidian distance between two vektors (arr_1 and arr_2) with length: 'dimension'
def euclidDist(arr_1,arr_2,dimension):
	distance=0
	for i in range(dimension):
		distance += ( (arr_1[i] - arr_2[i])**2 )
	return np.sqrt(distance)


# Determines the k nearest neighbors from a training set (tr_Set) to a test point. returns a list of neighbors
def getNeighbors(tr_Set, test_point, k):
	distances = []
	dimension = len(test_point)-1
	for i in range(len(tr_Set)):
		dist = euclidDist(test_point, tr_Set[i], dimension)
		distances.append(dist)
	index_all = np.argsort(distances)
	index=[]
	for i in range(k-1):
		index.append(index_all[i])
	return index


# Determines class of test point. If there are an equal number of Signal ('s') and Background ('b') labels it assumes Background by default
def getClass(index, tr_Set_label, k):
	signal = []
	background = []
	for i in range(k-1):
		if tr_Set_label[index[i]]=='s':
			signal.append(1)
		elif tr_Set_label[index[i]]=='u':
			background.append(1)
	if len(signal)>len(background):
		label = 's'
	else:
		label = 'u'
	return label


def kNN(tr_Set, tr_Set_label, data, k):
	data_label=[]
	for i in range(len(data)):
		data_label.append(getClass(getNeighbors(tr_Set, data[i], k), tr_Set_label, k))
	return data_label


# compares the true labels to the determined lables and counts the number of true positives (tp) etc.
# also calculates purity, efficiency and significance
def analyse(true_label, data_label):
	tp = 0
	fp = 0
	tn = 0
	fn = 0
	for i in range(data_label):
		if true_label[i]==data_label[i]:
			if data_label[i]=='s':
				tp += 1
			elif data_label[i]=='u':
				tn += 1
		elif true_label[i]!=data_label[i]:
			if data_label[i]=='s':
				fp += 1
			elif data_label[i]=='u':
				fn += 1
	purity = tp/(tp + fp)
	efficiency = tp/(tp + fn)
	significance = (tp + tn)/(tp + tn + fp + fn)
	return purity, efficiency, significance

#d)
k = 10

signal = root_numpy.root2array("NeutrinoMC.root", treename="Signal_MC_Akzeptanz",branches=("AnzahlHits","x","y"))
background = root_numpy.root2array("NeutrinoMC.root", treename="Untergrund_MC",branches=("AnzahlHits","x","y"))

# define training Set
tr_Set_signal=[]
tr_Set_signal_label=[]
tr_Set_background=[]
tr_Set_background_label=[]


for i in range(5000):
	tr_Set_signal.append(signal[i])
	tr_Set_signal_label.append('s')
	tr_Set_background.append(background[i])
	tr_Set_background_label.append('u')

tr_Set = tr_Set_signal + tr_Set_background
tr_Set_label = tr_Set_signal_label + tr_Set_background_label

# define test set
test_Set_signal=[]
test_Set_background=[]
test_Set_signal_label=[]
test_Set_background_label=[]

for i in range(20000):
	test_Set_background.append(background[i+5000])
	test_Set_background_label.append('u')
	if i < 10000:
		test_Set_signal.append(signal[i+5000])
		test_Set_signal_label.append('s')

data = test_Set_signal + test_Set_background
true_label = test_Set_signal_label + test_Set_background_label

# do the thing:
data_label = kNN(tr_Set, tr_Set_label, data, k)

#test the thing
purity, efficiency, significance = analyse(true_label, data_label)

print("k = ",k," , AnzahlHits")
print("Reinheit: ",purity)
print("Effizienz: ",efficiency)
print("Signifikanz: ",significance)

#f)
k = 20

data_label = kNN(tr_Set, tr_Set_label, data, k)
purity, efficiency, significance = analyse(true_label, data_label)

print("k = ",k," , AnzahlHits")
print("Reinheit: ",purity)
print("Effizienz: ",efficiency)
print("Signifikanz: ",significance)

#e)

k=10

for i in range(len(signal)):
	signal[i][0]=np.log10(signal[i][0])
	background[i][0]=np.log10(background[i][0])

tr_Set_signal=[]
tr_Set_signal_label=[]
tr_Set_background=[]
tr_Set_background_label=[]


for i in range(5000):
	tr_Set_signal.append(signal[i])
	tr_Set_signal_label.append('s')
	tr_Set_background.append(background[i])
	tr_Set_background_label.append('u')

tr_Set = tr_Set_signal + tr_Set_background
tr_Set_label = tr_Set_signal_label + tr_Set_background_label

test_Set_signal=[]
test_Set_background=[]
test_Set_signal_label=[]
test_Set_background_label=[]

for i in range(20000):
	test_Set_background.append(background[i+5000])
	test_Set_background_label.append('u')
	if i < 10000:
		test_Set_signal.append(signal[i+5000])
		test_Set_signal_label.append('s')

data = test_Set_signal + test_Set_background
true_label = test_Set_signal_label + test_Set_background_label

data_label = kNN(tr_Set, tr_Set_label, data, k)

purity, efficiency, significance = analyse(true_label, data_label)

print("k = ",k," , log10(AnzahlHits)")
print("Reinheit: ",purity)
print("Effizienz: ",efficiency)
print("Signifikanz: ",significance)