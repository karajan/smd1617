using Distributions
using StatsBase



function KS_Wert(f_x::Array{Float64}, f_0::Array{Float64}, N::Int64)
    diff = abs(f_x - f_0)
    d_max = maximum(diff)
    n = N
    m = N
    testwert = sqrt((n*m)/(n+m)) * d_max
    return testwert
end


function KS_Alpha(alpha::Float64)
    K_alpha = sqrt(log(2/alpha) / 2)
    return K_alpha
end


function test(lam::Float64, bins::Int64, N::Int64)
    mu = lam
    sigma = sqrt(lam)
    untergrenze = mu - 5*sigma
    obergrenze = mu + 5*sigma

    f_x = rand(Poisson(lam), N)
    hist_x = fit(Histogram, f_x, linspace(untergrenze, obergrenze, bins+1))
    cum_x = cumsum(hist_x.weights)
    cum_x /= cum_x[end]  # normieren

    f_0 = round(rand(Normal(mu, sigma), N))
    hist_0 = fit(Histogram, f_0, linspace(untergrenze, obergrenze, bins+1))
    cum_0 = cumsum(hist_0.weights)
    cum_0 /= cum_0[end]  # normieren
    
    return KS_Wert(cum_x, cum_0, N)
end


function calc()
    size = 100
    N = 10000
    bins = 100
    lambdas = linspace(0.1, 20, size)
    
    ks = zeros(size)
    for (i, lam) in enumerate(lambdas)
        for j in 1:200
            ks[i] += test(lam, bins, N)
        end
    end
    ks /= 5
    return ks
end


calc()
