#! /usr/bin/env python
# -*- coding: UTF-8 -*-

import numpy as np
from sympy import *

a, b, v = symbols('a b v')
maxw = a * v**2 * exp(-b * v**2)

''' für N '''
print(integrate(maxw, (v, 0, oo)))

''' für a) '''
print(diff(maxw, v))

''' für b) '''
print(integrate(maxw*v, (v, 0, oo)))
