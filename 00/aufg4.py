#! /usr/bin/env python3
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt

plt.style.use("ggplot")


E = 50e6
m = 511
s = (2*E)**2
γ = E / m
β = np.sqrt(1 - γ**-2)
α = 1 / 137

def f1(θ):
    return α**2/s * ((2 + np.sin(θ)**2) / (1 - β**2 * np.cos(θ)**2))

def f2(θ):
    return α**2/s * ((2 + np.sin(θ)**2) / (1/γ**2 + β**2 * np.sin(θ)**2))


"""plotten von f(x)"""
# xwerte = np.logspace(-6, -4, 100000, dtype="f8")
# plt.semilogx(xwerte, f1(xwerte))
# plt.semilogx(xwerte, f2(xwerte))
# plt.semilogx(xwerte, f1(xwerte) - f2(xwerte))
xwerte = np.linspace(-1e-4, 1e-4, 100000, dtype="f4")
# plt.plot(xwerte, f1(xwerte))
plt.plot(xwerte, f2(xwerte))
# plt.plot(xwerte, f1(xwerte) - f2(xwerte))

# plt.ylim(-0.1, 1.1)
# plt.savefig("plots/04vergleich.pdf", bbox_inches="tight")

plt.show()
