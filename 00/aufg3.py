#! /usr/bin/env python3
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt

plt.style.use("ggplot")


def f(x):
    return (x**3 + 1/3) - (x**3 - 1/3)

def g(x):
    return ((3 + x**3/3) - (3 - x**3/3)) / x**3


# plotten von f(x)
xwerte = np.logspace(1, 8, 1000)
plt.semilogx(xwerte, f(xwerte))
plt.ylim(-0.1, 1.1)
# plt.savefig("plots/03f.pdf", bbox_inches="tight")

# plotten von g(x)
# xwerte = np.logspace(-1, -8, 1000)
# plt.semilogx(xwerte, g(xwerte))
# plt.ylim(-0.1, 1.4)
# # plt.savefig("plots/03g.pdf", bbox_inches="tight")

plt.show()
