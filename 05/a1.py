#Blatt 5 Aufgabe 1
#Wenn ihr eine Lösung habt, bitte eure statt dieser verwenden. Weiß nicht wo der Fehler ist, es kommt aber Müll raus. 
import numpy as np
import matplotlib.pyplot as plt
import root_numpy

p0 = root_numpy.root2array("zwei_populationen.root", "P_0_10000")
p1 = root_numpy.root2array("zwei_populationen.root", "P_1")

p0_X = [i[0] for i in p0]
p0_Y = [i[1] for i in p0]
p1_X = [i[0] for i in p1]
p1_Y = [i[1] for i in p1]

p0_x = np.array(p0_X,dtype=float)
p0_y = np.array(p0_Y,dtype=float)
p1_x = np.array(p1_X,dtype=float)
p1_y = np.array(p1_Y,dtype=float)

vp0 = np.array([(p0_x[0],p0_y[0])],dtype=float)
vp1 = np.array([(p1_x[0],p1_y[0])],dtype=float)

for i in range(1,len(p0_x)):
	vp0 = np.append(vp0,[(p0_x[i],p0_y[i])],axis=0)
	vp1 = np.append(vp1,[(p1_x[i],p1_y[i])],axis=0)

#Berechne Mittelwerte:
mean0 = np.array([np.mean(p0_x),np.mean(p0_y)],dtype=float)
mean1 = np.array([np.mean(p1_x),np.mean(p1_y)],dtype=float)

#Berechne Kovarianzmatrizen
cov0 = np.cov(p0_x,p0_y)
cov1 = np.cov(p1_x,p1_y)
#Kombinierte Kovarianzmatrix??
a = vp0
b = vp1
#Berechne Streumatrizen
s0 = np.zeros((2,2))
s1 = np.zeros((2,2))

for i in range(0,len(p0_x)):
	a[i] = vp0[i]-mean0
	b[i] = vp1[i]-mean1
	s0 = s0 + np.outer(a[i],a[i])
	s1 = s1 + np.outer(b[i],b[i])

c = mean0-mean1
sb = np.outer(c,c)

sw = s0+s1
sw_inv = np.linalg.inv(sw)

#Berechne Projektion:
proj = np.dot(sw_inv,c)
#Normierung
proj_n = proj/(np.sqrt(np.dot(proj,proj)))
#Projiziere Punkte:
projp0 = np.array(p0_x,dtype=float)
projp1 = np.array(p0_x,dtype=float)

for i in range(0,len(p0_x)):
	projp0[i] = np.dot(proj_n,vp0[i])
	projp1[i] = np.dot(proj_n,vp1[i])

plt.plot(projp0)
plt.plot(projp1)
plt.show()
