#! /usr/bin/env python
# -*- coding: UTF-8 -*-

import numpy as np
import matplotlib.pyplot as plt

plt.style.use("ggplot")


gewicht, groesse = np.loadtxt("Groesse-Gewicht.txt", unpack=True)
bins = [5, 10, 15, 20, 30, 50]


''' a) '''
f, ax = plt.subplots(2, 3, sharey=True)
for i in range(2):
    for j in range(3):
        ax[i,j].hist(gewicht, bins=bins[2*i + j])
        ax[i,j].set_xticks([50, 100, 150, 200])

plt.grid()
# plt.show()
plt.savefig("plots/gewicht.pdf", bbox_inches="tight")


''' b) '''
f, ax = plt.subplots(2, 3, sharey=True)
for i in range(2):
    for j in range(3):
        ax[i,j].hist(groesse, bins=bins[2*i + j])
        ax[i,j].set_xticks([1.6, 1.7, 1.8, 1.9, 2.0])

plt.grid()
# plt.show()
plt.savefig("plots/groesse.pdf", bbox_inches="tight")


''' c) '''
werte = np.random.rand(1e5) * 100
werte = np.log(werte)
f, ax = plt.subplots(2, 3, sharey=True)
ticks = [1, 10]
for i in range(2):
    for j in range(3):
        ax[i,j].hist(werte, bins=bins[2*i + j])
        ax[i,j].set_xticks(ticks)

plt.grid()
# plt.show()
plt.savefig("plots/log.pdf", bbox_inches="tight")

