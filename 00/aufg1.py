#! /usr/bin/env python3
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt

plt.style.use("ggplot")


def f_nativ(x):
    return (1 - x)**6

def f_ausm(x):
    return x**6 - 6*x**5 + 15*x**4 - 20*x**3 + 15*x**2 - 6*x + 1

def f_horner(x):
    return 1 + x*(-6 + x*(15 + x*(-20 + x*(15 + x*(-6 + x)))))


xwerte = np.linspace(0.999, 1.001, 1000)


# einen Plot für den nativen Plot
plt.plot(xwerte, f_nativ(xwerte), label="nativ")
plt.xlim(0.9985, 1.0015)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
plt.legend()
plt.savefig("plots/01nativ.pdf", bbox_inches="tight")

plt.cla()

# und einen für die anderen im Vergleich
plt.plot(xwerte, f_ausm(xwerte), label="ausmultipliziert")
plt.plot(xwerte, f_horner(xwerte), label="Horner")
plt.plot(xwerte, f_nativ(xwerte), label="nativ")
plt.xlim(0.9985, 1.0015)
ax = plt.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
plt.legend()
plt.savefig("plots/01horner+ausm.pdf", bbox_inches="tight")
# plt.show()
