#Blatt 4 A1

import numpy as np
import matplotlib.pyplot as plt
import root_numpy
# import ROOT

#Erzeuge Population 0:
mean_0 = [0,3]
cov_0 = [[12.25,8.19],[8.19,6.76]]

p0 = np.random.multivariate_normal(mean_0,cov_0,10000)
p0_2 = np.random.multivariate_normal(mean_0,cov_0,1000)

x0 = np.zeros(10000)
y0 = np.zeros(10000)
x0_2 = np.zeros(1000)
y0_2 = np.zeros(1000)
x1 = np.zeros(10000)
y1 = np.zeros(10000)

#Population 1:
for n in range(0,10000):
	x1[n] = np.random.normal(6,3.5)
	y1[n] = np.random.normal((-0.5+0.6*x1[n]),1)

#Aufspalten von P0 in x- und y-Anteil
for (n,c) in enumerate(p0):
	x0[n] = c[0]
	y0[n] = c[1]

for (n,c) in enumerate(p0_2):
	x0_2[n] = c[0]
	y0_2[n] = c[1]

#Bilde Gesamtheit beider Populationen
ges_x = np.append(x0,x1)
ges_y = np.append(y0,y1)

#Ergebnisse ausgeben:
#Mittelwert:
print("Stichproben-Mittelwert:\nP0 (x): ",np.mean(x0),"\nP0 (y): ",np.mean(y0),
"\nP1 (x): ",np.mean(x1),"\nP1 (y): ",np.mean(y1),
"\nGesamt (x): ",np.mean(ges_x),"\nGesamt (y): ",np.mean(ges_y))

#Varianz:
print("Stichproben-Varianz:\nP0 (x): ",np.std(x0),"\nP0 (y): ",np.std(y0),
"\nP1 (x): ",np.std(x1),"\nP1 (y): ",np.std(y1),
"\nGesamt (x): ",np.std(ges_x),"\nGesamt (y): ",np.std(ges_y))

#Korrelationskoeffizienten (als Matritzen):
print("Stichproben-Korrelationskoeffizient:\nP0:\n",np.corrcoef(x0,y0),
"\nP1:\n",np.corrcoef(x1,y1),"\nGesamt:\n",np.corrcoef(ges_x,ges_y))

#Kovarianzen (als Matritzen):
print("Stichproben-Kovarianz:\nP0:\n",np.cov(x0,y0),
"\nP1:\n",np.cov(x1,y1),"\nGesamt:\n",np.cov(ges_x,ges_y))

#Plotten:
plt.scatter(x0,y0,color='red',edgecolor='black',label='P0')
plt.scatter(x1,y1,color='blue',edgecolor='black',label='P1')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc="upper left")
plt.savefig('A1_scatter.pdf',size=10)
# plt.show()

#Dinge in ROOT-Files schreiben:
# root_file = ROOT.TFile("zwei_populationen.root","RECREATE")

data = np.empty(len(x0), dtype=[("x", float)])
data["x"] = x0
root_numpy.array2root(data,"zwei_populationen.root",treename="P_0_10000",mode="recreate")
data = np.empty(len(y0), dtype=[("y", float)])
data["y"] = y0
root_numpy.array2root(data,"zwei_populationen.root","P_0_10000","update")
data = np.empty(len(x0_2), dtype=[("x0_2", float)])
data["x0_2"] = x0_2
root_numpy.array2root(data,"zwei_populationen.root","P_0_1000","update")
data = np.empty(len(y0_2), dtype=[("y0_2", float)])
data["y0_2"] = y0_2
root_numpy.array2root(data,"zwei_populationen.root","P_0_1000","update")
data = np.empty(len(x1), dtype=[("x", float)])
data["x"] = x1
root_numpy.array2root(data,"zwei_populationen.root","P_1","update")
data = np.empty(len(y1), dtype=[("y", float)])
data["y"] = y1
root_numpy.array2root(data,"zwei_populationen.root","P_1","update")


