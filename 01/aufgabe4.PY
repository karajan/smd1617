

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as scs

x = np.linspace(-6, 6, 2000)
y = np.linspace(-3, 3, 2000)

xx, yy = np.meshgrid(x, y)

XX = xx.flatten()
YY = yy.flatten()

vals = list(zip(XX, YY))
gaus_pdf = scs.multivariate_normal.pdf(
    vals, mean=[0, 0], cov=[[12.25, -4.2], [-4.2, 2.25]])

zz = gaus_pdf.reshape(xx.shape)

plt.pcolormesh(xx, yy, zz, cmap="pink")
plt.savefig("2dgaus.png", dpi=150)