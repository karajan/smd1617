#! /usr/bin/env python3
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt

# plt.style.use("ggplot")


x1 = np.arange(1, 101)

x2 = np.zeros((100, 8))

x2[:,0] = x1 / 100
x2[:,1] = np.random.rand(100)
x2[:,2] = np.random.rand(100) * 10
x2[:,3] = np.random.rand(100) * 10 + 20
x2[:,4] = np.random.randn(100)
x2[:,5] = np.random.normal(5, 2, 100)
x2[:,6] = x2[:,0]**2
x2[:,7] = np.cos(x2[:,0])


f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)

ax1.plot(x2[:,0], x2[:,6], "ro", ms=20, label="x^2")
ax1.set_xlabel("x")
ax1.set_ylabel("y")
ax1.set_title("Plot1")
ax1.legend()
ax1.grid()

ax2.plot(x2[:,0], x2[:,7], "gs", ms=20, label="cos(x)")
ax2.legend()


plt.show()
