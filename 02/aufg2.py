#! /usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


class Randomator():
    import time
    # use the current time in milliseconds as default seed
    seed = int(round(time.time() * 1000))
    a = 1601
    b = 3456
    m = 10000

    def get_rand(self):
        """ Return random number computed with linear-congruential
        algorithm. """

        self.seed = (self.a * self.seed + self.b) % self.m
        return self.seed


if __name__ == '__main__':
    import ROOT
    rnd_gen = Randomator()
    root_rnd = ROOT.TRandom()
    # plot histogram for different seeds
    for seed in [0, 1000, 10000]:
        rnd_gen.seed = seed
        rands = [rnd_gen.get_rand() for i in range(0, 10000)]
        plt.hist(rands, bins=30, edgecolor='white')
        plt.xlabel('$x$')
        plt.ylabel('Anzahl Ereignisse / \num{{{}}}'.format(10000/30))
        plt.savefig('plots/hist-seed-{}.pdf'.format(seed))
        plt.clf()

        root_rnd.SetSeed(seed)
        rootnum = [root_rnd.Rndm() for i in range(0, 10000)]
        plt.hist(rands, bins=30, edgecolor='white')
        plt.xlabel('$x$')
        plt.ylabel("Anzahl Ereignisse / \num{{{}}}".format(10000/30))
        plt.savefig('plots/hist-root-seed-{}.pdf'.format(seed))
        plt.clf()

    # plot pairs of two randoms, for each, own linear congruential
    # and ROOTs TRandom->Rndm()
    for n in np.logspace(1, 4, 4, dtype=int):
        rands_py = np.array([[rnd_gen.get_rand(), rnd_gen.get_rand()] for i
                in range(0, n)]) / 10000
        rands_root = np.array([[root_rnd.Rndm(), root_rnd.Rndm()] for i
                in range(0, n)])
        for t, r in zip(['python', 'root'], [rands_py, rands_root]):
            plt.scatter(r[:, 0], r[:, 1], s=1, marker='.',
                    linewidths=1, facecolor='black')
            plt.xlim(0, 1)
            plt.ylim(0, 1)
            plt.xlabel('x')
            plt.ylabel('y')
            plt.savefig('plots/scatter-2d-{}-{}.pdf'.format(t, n))
            plt.clf()

    # plot triplets of randoms
    for n in np.logspace(1, 3, 3, dtype=int):
        rands_py = np.array([[rnd_gen.get_rand(), rnd_gen.get_rand(),
                rnd_gen.get_rand()] for i in range(0, n)]) / 10000
        rands_root = np.array([[root_rnd.Rndm(), root_rnd.Rndm(),
                root_rnd.Rndm()] for i in range(0, n)])
        for t, r in zip(['python', 'root'], [rands_py, rands_root]):
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(r[:, 0], r[:, 1], r[:, 2])
            # set nice viewing-angles
            ax.azim = 17
            ax.elev = 21
            # and limits
            ax.set_xlim(0, 1)
            ax.set_ylim(0, 1)
            ax.set_zlim(0, 1)
            fig.savefig('plots/scatter-3d-{}-{}.pdf'.format(t, n))
