#! /usr/bin/env python3
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt

plt.style.use("ggplot")


def f(x):
    return (np.sqrt(9 - x) - 3) / x


# plotten von f(x)
xwerte = np.logspace(-1, -20, 1000)
plt.semilogx(xwerte, f(xwerte))
plt.ylim(-0.5, 0.05)
plt.savefig("plots/02grenzwert.pdf", bbox_inches="tight")

# plt.show()
