#! /usr/bin/env python
# -*- coding: UTF-8 -*-

import numpy as np
from scipy.stats import chi2



messwerte = np.array([31.6, 32.2, 31.9, 31.2, 31.3, 30.8, 31.3])
fehler = 0.5
vorausgesagt_a = 31.3
vorausgesagt_b = 30.7
signifikanz = 0.05
freiheitsgerade = messwerte.size

kritischer_wert = chi2.isf(signifikanz, freiheitsgerade)


def calc_chi2(vorausgesagt, messwerte, fehler):
    return np.sum((vorausgesagt - messwerte)**2 / fehler**2)


chi2_a = calc_chi2(vorausgesagt_a, messwerte, fehler)
chi2_b = calc_chi2(vorausgesagt_b, messwerte, fehler)

print(chi2_a, chi2_a < kritischer_wert)
print(chi2_b, chi2_b < kritischer_wert)
